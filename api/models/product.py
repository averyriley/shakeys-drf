from django.db import models
from api.models import Catalog


class Product(models.Model):
    name = models.CharField(max_length=70, blank=False, default='')
    description = models.CharField(max_length=200, blank=False, default='')
    active = models.BooleanField(default=False)
    catalog = models.ForeignKey(Catalog, related_name='catalogs', on_delete=models.CASCADE)
    price = models.IntegerField(default=0)
    creation = models.DateTimeField(auto_now=True)
    image = models.ImageField(upload_to='media/', blank=True)
