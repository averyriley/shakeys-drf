from django.db import models


class Catalog(models.Model):
    name = models.CharField(max_length=70, blank=False, default='')
    description = models.CharField(max_length=200,blank=False, default='')
    active = models.BooleanField(default=False)
    creation = models.DateTimeField(auto_now=True)
