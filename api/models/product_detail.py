from django.db import models
from api.models import Catalog, Product


class ProductDetail(models.Model):
    name = models.CharField(max_length=70, blank=False, default='')
    description = models.CharField(max_length=200, blank=False, default='')
    active = models.BooleanField(default=False)
    product = models.ForeignKey(Product, related_name='products', on_delete=models.CASCADE)
    price = models.IntegerField()
    type = models.CharField(max_length=20, null=False)
    creation = models.DateTimeField(auto_now=True)
