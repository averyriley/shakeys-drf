from rest_framework import viewsets
from rest_framework import permissions
from api.serializers import CatalogSerializer
from api.models import Catalog


class CatalogViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Catalog.objects.all()
    serializer_class = CatalogSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]