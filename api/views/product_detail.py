from rest_framework import viewsets
from rest_framework import permissions

from api.models import ProductDetail
from api.serializers import ProductDetailSerializer


class ProductDetailViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = ProductDetail.objects.all()
    serializer_class = ProductDetailSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]