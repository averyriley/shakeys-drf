from .user import UserViewSet
from .group import GroupViewSet
from .catalog import CatalogViewSet
from .product import ProductViewSet
from .product_detail import ProductDetailViewSet
