from rest_framework import serializers
from api.models import Product
from api.serializers import CatalogSerializer


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    catalog = CatalogSerializer(read_only=True)
    catalog_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Product
        fields = ['url', 'name', 'description', 'active', 'catalog', 'catalog_id', 'creation', 'price', 'image']
        extra_kwargs = {
            'url': {'view_name': 'products-detail'}
        }
