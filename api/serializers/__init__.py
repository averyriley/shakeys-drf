from .group import GroupSerializer
from .user import UserSerializer
from .catalog import CatalogSerializer
from .product import ProductSerializer
from .product_detail import ProductDetailSerializer

