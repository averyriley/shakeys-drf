from rest_framework import serializers
from api.models import Catalog


class CatalogSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Catalog
        fields = ['url', 'id', 'name']
        extra_kwargs = {
            'url': {'view_name': 'catalogs-detail'}
        }
