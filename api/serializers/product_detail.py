from rest_framework import serializers
from api.models import Product
from api.serializers import CatalogSerializer, ProductSerializer


class ProductDetailSerializer(serializers.HyperlinkedModelSerializer):
    product = ProductSerializer(read_only=True)
    product_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Product
        fields = ['url', 'name', 'description', 'active', 'product', 'product_id', 'price', 'creation']
        extra_kwargs = {
            'url': {'view_name': 'products_detail-detail'}
        }
